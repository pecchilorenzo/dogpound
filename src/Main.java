public class Main {
    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());

        Labrador l= new Labrador("inEnglishPlease","White");
        System.out.println(l.getName()+"Weight is "+l.avgBreedWeight()+" and says "+l.speak());

        Yorkshire y= new Yorkshire("Elisabeth");
        System.out.println(y.getName()+" say "+y.speak());
    }
}