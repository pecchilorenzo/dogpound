/**
 Labrador.java
 A class derived from Dog that holds information about
 a labrador retriever. Overrides Dog speak method and includes
 information about average weight for this breed.
/**
 Labrador.java
 A class derived from Dog that holds information about
 a labrador retriever. Overrides Dog speak method and includes
 information about average weight for this breed.

 @TODO THIS FILE HAS ERRORS THAT MUST BE CORRECTED

 */

public class Labrador extends Dog
{
    private String color; //black, yellow, or chocolate?
    private int breedWeight = 75;
    /**
     * @param name
     * @param color
     */
    public Labrador(String name, String color) {
        super(name);
        this.color = color;
    }
    /**
     * Big bark -- overrides speak method in Dog
     * @return a stronger bark string
     */

    public String speak()
    {
        return "WOOF";
    }

    /**
     * Static function that outputs the average weight of the Breed.
     * DO NOT ALTER THIS METHODS.
     * @return weight
     */

    public int avgBreedWeight()
    {
        return breedWeight;
    }
}